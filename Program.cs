﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using TasksManagement.Tasks;

namespace TasksManagement
{
    class Program
    {

        static void Main()
        {
            _ = LoadData();
            Console.WriteLine("Добро пожаловать в Менеджер Задач!");
            PrintHelp();
            // Основной цикл программы.
            while (true)
            {
                List<string> command = new List<string>(CommandLineSplit(Console.ReadLine()));
                Func<string[], bool> method;
                
                if (command.Count == 0)
                {
                    Console.WriteLine("Ошибка! Введите команду");
                    continue;
                }
                string commandPrefix = command[0];
                command.RemoveAt(0); 
                
                if (commandPrefix == "user")
                {
                    method = WorkWithUsers;
                }
                else if (commandPrefix == "project")
                {
                    method = WorkWithProjects;
                }
                else if (commandPrefix == "task")
                {
                    method = WorkWithTasks;
                }
                else if (commandPrefix == "help")
                {
                    PrintHelp();
                    continue;
                }
                else if (commandPrefix == "exit")
                {
                    SyncDataAsync();
                    return;
                }
                else
                {
                    Console.WriteLine("Такой команды не существует." +
                        " Вы всегда можете воспользоваться командой help для просмотра списка команд" + Environment.NewLine);
                    continue;
                }
               
                bool? result = false;
                try
                { 
                    result = method?.Invoke(command.ToArray());
                }
                catch (IndexOutOfRangeException)
                {
                    Console.WriteLine("Неправильно введена команда. Вы можете воспользоваться командой /help," +
                                      " чтобы уточнить, как использовать команду");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                Console.WriteLine();
            }
        }

        // Восстановление состояния программы до перезапуска.
        private static async System.Threading.Tasks.Task LoadData()
        {
            using (FileStream fs = new FileStream("users.json", FileMode.OpenOrCreate))
            {
                users = await JsonSerializer.DeserializeAsync<Dictionary<string, User>>(fs);
            }

            using (FileStream fs = new FileStream("projects.json", FileMode.OpenOrCreate))
            {
                projects = await JsonSerializer.DeserializeAsync<Dictionary<string, Project>>(fs);
            }
        }

        // Сохранение состояния программы перед выходом.
        private static void SyncDataAsync()
        {
            using (FileStream fs = new FileStream("users.json", FileMode.OpenOrCreate))
            {
                 JsonSerializer.SerializeAsync(fs, users);
            }

            using (FileStream fs = new FileStream("projects.json", FileMode.OpenOrCreate))
            {
                 JsonSerializer.SerializeAsync(fs, projects);
            }
        }

        // Метод, разбивающий строку по пробелам, и оставляющий пробелы внути кавычев.
        // (для имён с пробелами)
        private static List<string> CommandLineSplit(string v)
        {
            var splitedList = new List<string>();
            var splitedArray = v.Split("\"");
            for (int i = 0; i < splitedArray.Length; i++)
            {
                if (i % 2 != 0)
                {
                    splitedList.Add(splitedArray[i].Trim());
                }
                else
                {
                    foreach (var substring in splitedArray[i].Split(" ", StringSplitOptions.RemoveEmptyEntries))
                    {
                        splitedList.Add(substring);
                    }
                }
            }
            return splitedList;
        }

        private static Dictionary<string, User> users = new Dictionary<string, User>();
        private static Dictionary<string, Project> projects = new Dictionary<string, Project>();

        // Печать справки.
        private static void PrintHelp()
        {
            Console.WriteLine(String.Join(Environment.NewLine, new List<string>()
                        {"Список команд:",
                        "user",
                        "\tadd \"name\" - добавить пользователя",
                        "\tremove \"name\" - удалить пользователя по имени",
                        "\tlist - список пользователей", Environment.NewLine,
                        "project",
                        "\tadd \"name\" - добавить проект",
                        "\tremove \"name\" - удалить проект по имени",
                        "\trename \"old_name\" \"new_name\"",
                        "\tlist - список проектов", Environment.NewLine,
                        "task",
                        "\tadd \"project_name\" \"task_name\" \"task_type(epic/story/task/bug)\" - добавление новой задачи указанного типа",
                        "\taddsubtask \"project_name\" \"epic_task_name\" \"subtask_name\" - добавление существующей задачи типа task или story в подзадачи к epic_task",
                        "\tremovesubtask \"project_name\" \"epic_task_name\" \"subtask_name\" - удаление подзадачи из задачи",
                        "\tremove \"project_name\" \"task_name\" - удаление задачи по имени",
                        "\tadddev \"project_name\" \"task_name\" \"dev_name\" - добавление к задаче исполнителя по имени",
                        "\tremovedev \"project_name\" \"task_name\" \"dev_name\" - удаление у задачи исполнителя по имени",
                        "\tnextstatus \"project_name\" \"task_name\" - изменить статус задачи",
                        "\tlist \"project_name\" - просмотр списка задач",
                        "\t\t(опционально флаги -o для открытых задач, -f для завершенных, -w для задач в работе",
                        Environment.NewLine, "exit - выход из программы, сохранение состояния",
                        "Для использования имен из нескольких слов используйте кавычки: \"длинное имя\"",
                        "ВНИМАНИЕ! Сохранение данных происходит только при закрытии программы командой exit"}));
            Console.WriteLine();
        }

        // Обработка команд для работы с пользователями.
        static bool WorkWithUsers(string[] command)
        {
            if (command[0] == "add")
            {
                if (users.ContainsKey(command[1]))
                {
                    throw new ArgumentException("Пользователь с таким именем уже существует");
                }
                users.Add(command[1], new User(){ Name = command[1] });
                Console.WriteLine("Пользователь успешно добавлен.");
            }
            else if (command[0] == "remove")
            {
                if (users.ContainsKey(command[1]))
                {
                    users.Remove(command[1]);
                    Console.WriteLine("Пользователь успешно удалён.");
                }
                else throw new ArgumentException("Пользователя с таким именем не существует");
            }
            else if (command[0] == "list")
            {
                if (users.Count == 0)
                {
                    Console.WriteLine("В системе пока не зарегистрованно ни одного пользователя");
                }
                else
                {
                    Console.WriteLine("Пользователи зарегестрированные в системе:");
                }
                foreach (User user in users.Values) Console.WriteLine(user);
            }
            else
            {
                return false;
            }
            return true;
        }

        // Обработка команд для работы c проектами.
        static bool WorkWithProjects(string[] command)
        {
            if (command[0] == "add")
            {
                if (projects.ContainsKey(command[1]))
                {
                    throw new ArgumentException("Проект с таким названием уже существует");
                }
                projects.Add(command[1], new Project() { Name = command[1] });
                Console.WriteLine("Проект успешно создан.");
            }
            else if (command[0] == "remove")
            {
                if (projects.ContainsKey(command[1]))
                {
                    projects.Remove(command[1]);
                    Console.WriteLine("Проект успешно удалён.");
                }
                else throw new ArgumentException("Ошибка! Проекта с таким именем не существует.");
            }
            else if (command[0] == "rename")
            {
                if (projects.ContainsKey(command[2]))
                {
                    throw new ArgumentException("Ошибка! Проект с таким именем уже существует.");
                }
                
                if (projects.ContainsKey(command[1]))
                {
                    projects[command[1]].Name = command[2];
                    projects.Add(command[2], projects[command[1]]);
                    projects.Remove(command[1]);
                }
                else throw new ArgumentException("Ошибка. Не удалось найти проект с указанным именем.");
                Console.WriteLine("Проект успешно переименован");

            }
            else if (command[0] == "list")
            {
                if (projects.Count == 0)
                {
                    Console.WriteLine("Вы пока не создали ни одного проекта");
                }
                else
                {
                    Console.WriteLine("Созданные раннее проекты:");
                }
                foreach (Project project in projects.Values) Console.WriteLine(project);
            }
            else
            {
                return false;
            }
            return true;
        }

        // Обработка всех команд для работы задачами.
        static bool WorkWithTasks(string[] command)
        {
            if (!projects.ContainsKey(command[1]))
            {
                throw new ArgumentException("Ошибка! Проекта с указанным именем не существует.");
            }
            Project project = projects[command[1]];

            // Если работа ведется с проектом.
            if (command[0] == "add")
            {
                project.AddTask(command[2], command[3]);
                Console.WriteLine("Задача успешно добавлена");
                return true;
            }
            else if (command[0] == "remove")
            {
                project.RemoveTask(command[2]);
                Console.WriteLine("Задача успешно удалена");
                return true;
            }
            else if (command[0] == "list")
            {
                project.PrintTasks(command.Length > 2? command[2]: null);
                return true;
            }
            // Работа ведется уже с существующей задачей внутри проекта.
            if (!project.Tasks.ContainsKey(command[2]))
            {
                throw new ArgumentException("Ошибка! В данном проекте нет задачи с заданным именем.");
            }
            BaseTask task = project.Tasks[command[2]];

            bool result = WorkWithExsistTask(project, task, command);
            return result;
        }

        // Обработка команд, относящихся к работе с существующей задачей.
        static bool WorkWithExsistTask(Project project, BaseTask task, string[] command)
        {
            if (command[0] == "addsubtask" || command[0] == "removesubtask")
            {
                if (!project.Tasks.ContainsKey(command[3]))
                {
                    throw new ArgumentException("Ошибка! В данном проекте нет указанной подзадачи.");
                }
                if (task is Epic epic)
                {
                    if (command[0] == "addsubtask")
                    {
                        epic.AddSubTask(project.Tasks[command[3]]);
                    }
                    else
                    {
                        epic.RemoveSubTask(project.Tasks[command[3]]);
                    }
                }
                else
                {
                    throw new ArgumentException("Ошибка! Невозможно добавить подзадачу к указанной задаче, так как она не является задачей типа Epic");
                }
            }
            else if (command[0] == "adddev")
            {
                if (!users.ContainsKey(command[3]))
                {
                    throw new ArgumentException("Ошибка! Указанного пользователя нет в системе");
                }
                task.AddDeveloper(users[command[3]]);
            }
            else if (command[0] == "removedev")
            {
                if (!users.ContainsKey(command[3]))
                {
                    throw new ArgumentException("Ошибка! Указанного пользователя нет в системе");
                }
                task.RemoveDeveloper(users[command[3]]);
            }
            else if (command[0] == "nextstatus")
            {
                task.NextStatus();
            }
            else
            {
                return false;
            }
            
            return true;
        }
    }
}
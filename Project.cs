﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TasksManagement.Tasks;

namespace TasksManagement
{
    // Класс проекта. Содержит задачи.
    public class Project
    {
        string name;
        public string Name { 
            get =>  name;
            set { name = value; }
        }

        public Project() { }

        // Задачи этого проекта.
        public Dictionary<string, BaseTask> Tasks { get; set; } = new Dictionary<string, BaseTask>();
        int maxTasksCount = 10;

        // Метод, выводящий все задачи проекта.
        internal void PrintTasks(string type)
        {
            // Флаги, тип задач, который нужно выводить.
            if (type == "-o")
            {
                type = TasksManagement.Tasks.TaskStatus.openTaskStatus;
            }
            else if (type == "-f")
            {
                type = TasksManagement.Tasks.TaskStatus.finishedTaskStatus;
            }
            else if (type == "-w")
            {
                type = TasksManagement.Tasks.TaskStatus.inWorkTaskStatus;
            }
            else if (type != null)
            {
                throw new ArgumentException("Ошибка! Использован недопустимый флаг.");
            }

            if (Tasks.Count == 0)
            {
                Console.WriteLine("В выбранном проекте пока нет задач");
            }
            else
            {
                Console.WriteLine("Задачи проекта:");
            }

            foreach (BaseTask task in Tasks.Values)
            {
                if (type != null && task.status.Status != type) continue;
                Console.WriteLine(task);
            }
        }

        // Удаление задачи.
        internal void RemoveTask(string taskName)
        {
            if (!Tasks.ContainsKey(taskName))
            {
                throw new ArgumentException("Ошибка! Задачи с указанным именем не существует в данном проекте");
            }
            Tasks.Remove(taskName);
        }

        // Добавление задачи.
        internal void AddTask(string taskName, string type)
        {
            if (Tasks.Count == maxTasksCount)
            {
                throw new IndexOutOfRangeException("Ошибка! В задачу добавлено максимально число задач.");
            }
            if (Tasks.ContainsKey(taskName))
            {
                throw new ArgumentException("Ошибка! Задача с указанным именем уже добавлена в этот проект");
            }
            BaseTask task;
            if (type == "epic")
            {
                task = new Epic(taskName);
            }
            else if (type == "story")
            {
                task = new Story(taskName);
            }
            else if (type == "task")
            {
                task = new Tasks.Task(taskName);
            }
            else if (type == "bug")
            {
                task = new Bug(taskName);
            }
            else
            {
                throw new ArgumentException("Ошибка! Указанный тип задач не существует.");
            }
            Tasks.Add(taskName, task);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}

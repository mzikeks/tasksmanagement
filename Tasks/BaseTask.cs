﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TasksManagement.Tasks
{
    // Базовый класс, от которого наследуются все типы задач.
    // Реализует интерфейс IAssingable
    public class BaseTask: IAssingable
    {
        public string Name { get; set; }

        public string creationTime { get; set; }
        // Если текущая задача чья-то подзадача.
        public BaseTask parent { get; set; } = null;
        public int maxTaskDevs { get; protected set; }

        public BaseTask() { }

        public BaseTask(string taskName)
        {
            creationTime = DateTime.Now.ToString();
            Name = taskName;
        }

        public List<User> developers { get; set; } = new List<User>();

        // В работе/открытая/завершена.
        public TaskStatus status { get; set; } = new TaskStatus();

        // Добавление разработчика.
        public void AddDeveloper(User developer)
        {
            if (developers.Count >= maxTaskDevs)
            {
                throw new ArgumentException("Ошибка! К задаче данного типа невозможно добавить больше исполнителей.");
            }
            if (developers.Contains(developer))
            {
                throw new ArgumentException("Ошибка! Указанный пользователь уже является исполнителем этой задачи");
            }
            developers.Add(developer);
            Console.WriteLine("Разработчик успешно добавлен к задаче.");
        }

        // Удаление разработчика.
        public void RemoveDeveloper(User developer)
        {
            if (!developers.Contains(developer))
            {
                throw new ArgumentException("Ошибка! Указанный пользователь не является исполнителем данной задачи.");
            }
            developers.Remove(developer);
            Console.WriteLine("Разработчик успешно удален из задачи.");
        }

        // То, как будут печататься задачи.
        public override string ToString()
        {
            var SB = new StringBuilder();
            SB.Append($"Задача {Name} типа {this.GetType().ToString().Split(".")[^1]}" +
                $" со статусом \"{status}\", создана {creationTime}");
            // Если является подзадачей.
            SB.Append(parent == null ? "" : $"(подзадача задачи {parent.Name})");
            SB.Append(Environment.NewLine + "Исполнители: ");
            foreach (User user in developers)
            {
                SB.Append(user + ", ");
            }
            SB.Remove(SB.Length-2, 2);
            SB.Append(Environment.NewLine);

            return SB.ToString();
        }

        // Смена статуса.
        internal void NextStatus()
        {
            if (status.Status == TaskStatus.finishedTaskStatus)
            {
                throw new ArgumentException("Ошибка! Невозможно изменить статус, задача уже завершена");
            }
            status.Next();
            Console.WriteLine("Статус успешно изменен.");
        }
    }

    // Интерфейс добавления разработчиков.
    interface IAssingable
    {
        List<User> developers { get; }
        void AddDeveloper(User developer);
        void RemoveDeveloper(User developer);
    }
}

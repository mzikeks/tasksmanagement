﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TasksManagement.Tasks
{
    [Serializable]
    public class Bug: BaseTask, IAssingable
    {
        public Bug(string taskName)
            : base(taskName)
        {
            maxTaskDevs = 1;
        }

        public Bug() { }
    }
}

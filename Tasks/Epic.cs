﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TasksManagement.Tasks
{
    [Serializable]
    public class Epic : BaseTask, IAssingable
    {
        // Словарь подзадач.
        public Dictionary<string, BaseTask> subTasks = new Dictionary<string, BaseTask>();

        public Epic(string taskName)
            : base(taskName)
        {
            maxTaskDevs = 1000;
        }

        public Epic() { }

        // Добавление подзадачи.
        internal void AddSubTask(BaseTask subtask)
        {
            if (!(subtask is Story) && !(subtask is Task))
            {
                throw new ArgumentException("Ошибка! Невозможно добавить указанную задачу как подзадачу. " +
                    "Подзадачей могут быть только задачи типа Story или Task");
            }
            subTasks.Add(subtask.Name, subtask);
            subtask.parent = this;
            Console.WriteLine("Подзадача успешно добавлена.");
        }

        // Удаление подзадачи.
        internal void RemoveSubTask(BaseTask subtask)
        {
            if (!subTasks.ContainsKey(subtask.Name))
            {
                throw new ArgumentException("Ошибка! Указанной задач нет в списке подзадач.");
            }
            subTasks.Remove(subtask.Name);
            subtask.parent = null;
            Console.WriteLine("Подзадача успешно удалена.");
        }
    }
}

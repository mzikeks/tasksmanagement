﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TasksManagement.Tasks
{
    [Serializable]
    public class Story: BaseTask, IAssingable
    {
        public Story(string taskName)
            :base(taskName)
        {
            maxTaskDevs = 300;
        }
        public Story() { }
    }
}

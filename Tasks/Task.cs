﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TasksManagement.Tasks
{
    [Serializable]
    public class Task: BaseTask, IAssingable
    {
        public Task(string taskName)
            : base(taskName)
        {
            maxTaskDevs = 1;
        }

        public Task() { }
    }
}

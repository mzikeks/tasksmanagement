﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TasksManagement.Tasks
{
    [Serializable]
    // Класс отвечающий за состояние задачи.
    public class TaskStatus
    {
        public static readonly string openTaskStatus = "открытая";
        public static readonly string inWorkTaskStatus = "в работе";
        public static readonly string finishedTaskStatus = "завершена";
        public string Status { get; private set; }

        public TaskStatus()
        {
            this.Status = "открытая";
        }

        // Статусы меняются линейно, метод меняет текущий статус.
        public string Next()
        {
            if (Status == openTaskStatus)
            {
                this.Status = inWorkTaskStatus;
            }
            else if (Status == inWorkTaskStatus)
            {
                this.Status = finishedTaskStatus;
            }
            return Status;
        }

        public override string ToString()
        {
            return Status;
        }
    }
}
